import {app} from '../app';
import chai from 'chai';
import chaiHttp from 'chai-http';

import 'mocha';

chai.use(chaiHttp);

describe('Testing payment endpoints', () => {
  it('should return bad request', (done) => {
    chai.request(app).get('/payments')
    .then(res => {
        chai.expect(res.status).to.equal(400);
        done();
    });
  });

  it('should return payment not found', (done) => {
    chai.request(app).get('/payments')
    .send({
      contractId: 12
    }).then(res => {
      chai.expect(res.status).to.eql(404);
      done();
    });

  });
  it('should return payment of id=1 and value=100', (done) => {
    chai.request(app).post('/payments')
    .send({
      contractId: 12,
      value: 100,
      description: "Rent Payment",
      isImported: false
    })
    .then(res => {
      chai.expect(res.status).to.eql(200);
      done();
    })
  });

  it('should return payment of id=2 and value=-100', (done) => {
    chai.request(app).post('/payments')
    .send({
      contractId: 12,
      value: -100,
      description: "Rent to be paid",
      isImported: false
    })
    .then(res => {
      chai.expect(res.status).to.eql(200);
      chai.expect(res.body.id).to.eql(2);
      chai.expect(res.body.value).to.eql(-100);

      done();
    })
  });

  it('should return 2 payment of sum=0', () => {
    return chai.request(app).get('/payments')
    .send({
      contractId: 12
    }).then(res => {
      chai.expect(res.status).to.eql(200);
      chai.expect(res.body.items).to.have.lengthOf(2);
      chai.expect(res.body.sum).to.eql(0);
    })
  });

  it('should update payment of id=2 with value=-50', (done) => {
    chai.request(app).put('/payments/2')
    .send({
      value: -50
    })
    .then(res => {
      chai.expect(res.status).to.eql(200);
      chai.expect(res.body.id).to.eql(2);
      chai.expect(res.body.value).to.eql(-50);

      done();
    })
  });

  it('should return 2 payment of sum=50', () => {
    return chai.request(app).get('/payments')
    .send({
      contractId: 12
    }).then(res => {
      chai.expect(res.status).to.eql(200);
      chai.expect(res.body.items).to.have.lengthOf(2);
      chai.expect(res.body.sum).to.eql(50);
    })
  });

  it('should delete payment of id=2', (done) => {
    chai.request(app).delete('/payments/2')
    .then(res => {
      chai.expect(res.status).to.eql(200);
      done();
    });

    it('should return 1 payment of sum=100', () => {
      return chai.request(app).get('/payments')
      .send({
        contractId: 12
      }).then(res => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body.items).to.have.lengthOf(1);
        chai.expect(res.body.sum).to.eql(100);
      })
    });

  });
})
