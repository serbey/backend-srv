import { NextFunction, Request, Response } from 'express';

function ErrorMiddleware(error: any, request: Request, response: Response, next: NextFunction) {
    // handle bad request error
    if(error.httpCode === 400) {
        response.status(400).send({
            message: `Missing or invalid ${error.errors.map((x: any) => x.property)}`
        })
    } else {
        // handle 500 error, HttpException thrown error and treat any other error as 500
        const status = error.httpCode || 500;
        const message = error.message || 'Something went wrong';
        response.status(status).send({
            message
        })
    }
}
export default ErrorMiddleware;