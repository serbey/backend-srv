import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreatePaymentTable1607296887442 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        return await queryRunner.createTable(new Table({
            name: "payments",
            columns: [
                {
                    name: "id",
                    type: "integer",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "contractId",
                    type: "integer",
                    isNullable: false
                },
                {
                    name: "description",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "value",
                    type: "integer",
                    isNullable: false,
                },
                {
                    name: "time",
                    type: "datetime",
                    isNullable: false
                },
                {
                    name: "isImported",
                    type: "boolean",
                    isNullable: false
                },
                {
                    name: "createdAt",
                    type: "datetime",
                    isNullable: false
                },
                {
                    name: "updatedAt",
                    type: "datetime",
                    isNullable: false
                },
                {
                    name: "isDeleted",
                    type: "boolean",
                    isNullable: false
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        return await queryRunner.dropTable("payments");
    }

}
