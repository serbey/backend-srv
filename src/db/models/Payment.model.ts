import {
    BaseEntity,
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from "typeorm";

@Entity('payments')
export class Payment extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    contractId: number;
    @Column()
    description: string;
    @Column()
    value: number;
    @Column()
    time: Date;
    @Column()
    isImported: boolean;
    @Column()
    createdAt: Date;
    @Column()
    updatedAt: Date;
    @Column()
    isDeleted: boolean;
}