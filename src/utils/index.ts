import { LessThanOrEqual, MoreThanOrEqual, Not, Raw, IsNull } from "typeorm";

// function used to have date format comparable with dates in sqlite
export function toSqlDateString(date: Date): string {
    return `${date.toString().substring(0, 10)} 00:00:00.000`;
}

// function used to create the time field from the createdAt or updatedAt
export function zeroOutTime(date: Date): Date {
    return new Date(new Date(date).setUTCHours(0,0,0,0));
}

// function used to have a dynamic query based on availability of startDate and endDate
export function getTimeCondition(startDate: Date, endDate: Date): any {
    if(startDate && endDate) {
        return Raw(
            alias =>`${alias} BETWEEN :startDate AND :endDate`,
            {startDate: toSqlDateString(startDate), endDate: toSqlDateString(endDate)});
    } else if(startDate) {
        return MoreThanOrEqual(toSqlDateString(startDate));
    } else if(endDate) {
        return LessThanOrEqual(toSqlDateString(endDate))
    } else {
        return Not(IsNull())
    }
}