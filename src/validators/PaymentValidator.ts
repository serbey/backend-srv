/* tslint:disable:max-classes-per-file */
import {IsBoolean, IsISO8601, IsInt, IsNumber, IsOptional, IsString} from 'class-validator';

/**
 * This file contains the validation requirement for every body in Payment APIs
 * A 400 (bad request) error would pop in case the body request is missing
 */


export class AddPaymentBody {
    @IsInt()
    contractId: number;
    @IsString()
    description: string;
    @IsNumber()
    value: number;
    @IsBoolean()
    isImported: boolean;
}

export class GetPaymentsBody {
    @IsInt()
    contractId: number;
    @IsISO8601() @IsOptional()
    startDate: Date;
    @IsISO8601() @IsOptional()
    endDate: Date;
}

export class EditPaymentBody {
    @IsString() @IsOptional()
    description: string;
    @IsNumber() @IsOptional()
    value: number;
}
