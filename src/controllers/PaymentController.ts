import { JsonController, Param, Body, Get, Post, Put, Delete } from "routing-controllers";
import { AddPaymentBody, GetPaymentsBody, EditPaymentBody } from "../validators/PaymentValidator";
import {Payment} from "../db/models/Payment.model"
import {getTimeCondition, zeroOutTime} from "../utils"
import PaymentNotFoundException from "../exceptions/PaymentNotFoundException";

@JsonController()
export class PaymentController {

    @Post("/payments")
    async addPayment(@Body() body: AddPaymentBody) {
        // using AddPaymentBody, body is verified to have the required keys with correct type
        const payment = new Payment();
        const currentDate: Date = new Date();

        payment.contractId = body.contractId;
        payment.description = body.description;
        payment.value = body.value;
        payment.time = zeroOutTime(currentDate); // always get updated with updatedAt
        payment.isImported = body.isImported;
        payment.createdAt = currentDate;
        payment.updatedAt = currentDate;
        payment.isDeleted = false;

        await payment.save();

        return payment;
    };

    @Get("/payments")
    async getPayments(@Body() body: GetPaymentsBody) {
        // query payments by contractId with optional startDate and endDate
        const payments = await Payment.find({
            where: {
                contractId: body.contractId,
                isDeleted: false,
                time: getTimeCondition(body.startDate, body.endDate)
            }
        })

        // return 404 in case no payment was found for the requested parameters
        if (payments && payments.length) {
            const sum = payments.reduce((accumulator, currentPayment) => accumulator + currentPayment.value, 0);
            return {sum, items: payments};
        } else {
            throw new PaymentNotFoundException();
        }
    };

    @Delete("/payments/:id")
    async removePayment(@Param("id") id: number) {
        const payment = await Payment.findOne({
            where: {
                id,
                isImported: false, // imported payments can't be deleted
                isDeleted: false
            }
        });
        // update isDeleted to true
        if (payment) {
            payment.isDeleted = true;
            payment.updatedAt = new Date();
            await payment.save();
            return {message: 'Payment deleted'};
        } else {
            throw new PaymentNotFoundException();
        }
    };

    @Put("/payments/:id")
    async editPayment(@Param("id") id: number, @Body() body: EditPaymentBody) {
        // a payment can have it's value or it's description edited
        const payment = await Payment.findOne({
            where: {
                id,
                isImported: false, // imported payments can't be edited
                isDeleted: false
            }
        });
        if (payment) {
            const currentDate = new Date();
            if (body.value) {
                payment.value = body.value;
                payment.updatedAt = currentDate;
                payment.time = zeroOutTime(currentDate); // update payment time
            }
            if (body.description) { // changing description won't update the payment time
                payment.description = body.description;
                payment.updatedAt = currentDate;
            }
            await payment.save();
            return payment;
        } else {
            throw new PaymentNotFoundException();
        }
    };
}