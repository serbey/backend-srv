import HttpException from "./HttpException";

// customized exception for payment not found
class PaymentNotFoundException extends HttpException {
  constructor() {
    super(404, "Payment not found for the requested params");
  }
}

export default PaymentNotFoundException;