// default HttpException used to throw error
class HttpException extends Error {
    httpCode: number;
    message: string;
    constructor(httpCode: number, message: string) {
        super(message);
        this.httpCode = httpCode;
        this.message = message;
    }
}

export default HttpException;