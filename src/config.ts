const environment = process.env.ENV ? process.env.ENV : "dev"; // can take value of prod, dev & test

export default {
    ENV: environment,
    DATABASE: environment === "test" ? "database_test.db" : "database.db"
}
