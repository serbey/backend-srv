import "reflect-metadata"; // this shim is required
import { createExpressServer } from "routing-controllers";
import {connect} from "./db/db";
import ErrorMiddleware from "./middlewares/ErrorMiddleware"

// connect to SQLite
connect()

// creates express app instance with all controller routes and middleware
const app = createExpressServer({
   controllers: [__dirname + "/controllers/*.js"],
   defaultErrorHandler: false
});
app.use(ErrorMiddleware);

export {app};