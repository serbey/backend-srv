
# Summary

RESTful service to manage tenant's payment history

For the moment, it supports the following APIs:

`GET /payments` to fetch the list of payments for specific parameters:
```
{
	"contractId": 17689,
	"startDate": "2020-12-07T00:00:00.000Z", // optional
	"endDate": "2020-12-07", // optional
}
```
*dates can have any of the 2 format mentioned above and are optional to provide. Payments of the requested contractId are fetched and can also be filtered within a particular time frame using only startDate or only endDate or both.*

`POST /payments` to add a new payment to a contract using:
```
{
	"contractId": 17689,
	"description": "Rent payment",
	"value": 100,
	"isImported": false
}
```
*the entire body mentioned above is required to create payment.*

`PUT /payemnts/:id` to update an existing payment using its id with:
```
{
	"description": "Rent to be paid",
	"value": -100
}
```
*payment description or payment value can be updated and payment time gets updated in case an update of the payment value occured.*
*Updating contractId is not practical and in case it is needed, user should simply delete the payment and create a new one with correct contractId. Also it is assumed that imported payments can't be updated as its source is trusted.*

`DELETE /payments/:id` to delete a payment using its id
```
{

}
```
*imported payments can't be deleted*

# Application Launch

To launch the application, we should:

 1. Install all dependencies `npm install .`
 2. Create payments table in SQLite `npm run migration:run`
 3. Launch API server `npm run start`

Unit tests on endpoints can be checked using `npm run test`  
The application must be compiled in case it is not  
`npm run prebuild && npm run build` after `npm install .`