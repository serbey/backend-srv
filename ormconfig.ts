import config from "./src/config"

module.exports = {
  "type": "sqlite",
  "database": config.DATABASE,
  "name": "default",
  "synchronize": false,
  "logging": true,
  "migrationsTableName": "custom_migration_table",
  "entities": [
    "src/db/models/**/*.ts"
  ],
  "migrations": [
    "src/db/migrations/*.ts"
  ],
  "cli": {
    "entitiesDir": "src/db/models",
    "migrationsDir": "src/db/migrations"
  }
}